import {HttpClient} from '@angular/common/http';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {
    InjectionNames,
    BpmnModeler,
    PropertiesPanelModule,
    CamundaModdleDescriptor,
    ElementTemplates,
    OriginalPaletteProvider
} from './bpmn-js';
import {Translate} from './translate';
import {ModeladorPropsProvider} from './modelador-props-provider';
import {BPMNTipoElemento} from './bmpn-tipo-elemento.enum';
import {log} from 'util';

// @ts-ignore
@Component({
    selector: 'app-diagrama',
    templateUrl: 'diagrama.component.html',
    styleUrls: ['diagrama.component.scss']
})
export class DiagramaComponent implements OnInit, OnDestroy {

    titulo = 'POC - BPMN-JS';
    modeler: any;
    modeling: any;

    // add custom elements
    customElements = [
        {
            type: 'custom:triangle',
            id: 'CustomTriangle_1',
            x: 300,
            y: 300
        },
        {
            type: 'custom:connection',
            id: 'CustomConnection_1',
            source: 'CustomTriangle_1',
            target: 'Task_1',
            waypoints: [
                // ...
            ]
        }
    ];

    constructor(private http: HttpClient) {
    }

    ngOnInit(): void {

        // Criação do Modeler e PropertiesPanel
        this.modeler = new BpmnModeler({
            container: '#canvas',
            width: '99vw',
            height: '86vh',
            keyboard: { bindTo: document },
            additionalModules: [
                {[InjectionNames.elementTemplates]: ['type', ElementTemplates.elementTemplates[1]]},
                {[InjectionNames.propertiesProvider]: ['type', ModeladorPropsProvider]},
                {[InjectionNames.originalPaletteProvider]: ['type', OriginalPaletteProvider]},
                PropertiesPanelModule,
                {[InjectionNames.translate]: ['value', Translate.translate]},
            ],
            propertiesPanel: {
                parent: '#properties'
            },
            moddleExtensions: {
                camunda: CamundaModdleDescriptor
            }
        });
        // this.modeler.addCustomElements(customElements);
        this.carregar();
    }

    ngOnDestroy(): void {
        this.modeler.destroy();
    }

    private carregar(): void {

        const url = '/assets/inicial.bpmn';

        this.http
            .get(url, { headers: {observe: 'response'}, responseType: 'text' })
            .subscribe(
                x =>  this.modeler.importXML(x, this.handleError),
                this.handleError
            );


        // const elementRegistry = this.modeler.get('elementRegistry');
        // this.modeling = this.modeler.get('modeling');
        // console.log('elementRegistry', elementRegistry);
        // const elementColor = elementRegistry.get('Process_1');
        // this.modeling.setColor([ elementColor ], {
        //    stroke: 'green',
        //    fill: 'rgba(0, 80, 0, 0.4)'
        // });
    }

    private salvar(): void {
        this.modeler.saveXML((err: any, xml: any) => console.log('XML salvo: ', err, xml));
    }

    private handleError(err: any): void {

        if (err) {
            console.warn('Ups, error: ', err);
        }
    }
    // private pintar() {
    //     const canvas = this.modeler.get('canvas');
    //     canvas.addMarker('Teste', 'highlight');
    // }

    mostrar() {

        let xmlDocument;

        this.modeler.saveXML({ format: false }, (err: any, pXML: string) => {
            xmlDocument = (new DOMParser()).parseFromString(pXML, 'application/xml');

            const elementsInicio = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.startXML);
            const elementsServico = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.serviceXML);
            const elementsRegra = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.exclusiveGatewayXML);
            const elementsEntrada = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.intermediateThrowXML);
            const elementsFim = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.endXML);
            const elementsProccess = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.processXML);
            const elementsScript = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.scriptXML);
            const elementsSaida = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.saidaXML);
            const elementsValidacaoDispositivo = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.sendXML);
            const elementsRemocaoPropriedade = xmlDocument.documentElement.getElementsByTagName(BPMNTipoElemento.manualXML);

            console.log('Tipos de atributos: ', xmlDocument.documentElement.getAttributeNames());

            console.log('elementsInicio: ', elementsInicio);
            console.log('elementsServico', elementsServico);
            console.log('elementsRegra', elementsRegra);
            console.log('elementsEntrada', elementsEntrada);
            console.log('elementsFim', elementsFim);
            console.log('elementsProccess', elementsProccess);
            console.log('elementsScript', elementsScript);
            console.log('elementsSaida', elementsSaida);
            console.log('elementsValidacaoDispositivo', elementsValidacaoDispositivo);
            console.log('elementsRemocaoPropriedade', elementsRemocaoPropriedade);
        });
    }
}
