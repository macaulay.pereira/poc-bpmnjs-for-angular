import translatePT from './translate-pt';

export class Translate {

    public static translate(template: any, replacements: any) {

        replacements = replacements || {};

        // translate
        template = translatePT[template] || template;

        // replace
        return template.replace(/{([^}]+)}/g, (_, key) => {
            return replacements[key] || '{' + key + '}';
        });
    }
}
