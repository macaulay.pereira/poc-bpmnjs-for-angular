import {EntryFactory, PropertiesActivator} from './bpmn-js';

/**
 * Classe responsável pelas alterações da estrutura do PropertiesProvider
 */
export class ModeladorPropsProvider extends PropertiesActivator {

    constructor(props) {
        super(props);
        PropertiesActivator.call(this, props);
    }

    private testeCheckBox = {};

    /**
     * @author Macaulay e André
     * Método que cria as abas personalizadas do PropertiesPanel
     */
    getTabs(element: any) {

        if ('bpmn:StartEvent'.toLowerCase() === element.type.toLowerCase()) {
            const entries = [EntryFactory.label({
                id: 'nome',
                labelText: 'Elemento início'
            })];

            // entries.unshift(
            //     EntryFactory.textBox({
            //         id: 'textoComponentEntrada',
            //         label: 'teste de texto de entrada',
            //         modelProperty: 'nome'
            //     }),
            //     EntryFactory.selectBox({
            //         id: 'selectBox ComponentEntrada',
            //         label: 'teste de selectBox',
            //         modelProperty: 'selectBox',
            //         selectOptions: [
            //             {name: 'valor - 1', value: 1},
            //             {name: 'valor - 2', value: 2},
            //             {name: 'valor - 3', value: 3},
            //             {name: 'valor - 4', value: 4},
            //         ]
            //         id: 'componenteEntrada',
            //         label: 'teste de texto início',
            //         modelProperty: 'jsonSaida'
            //     })
            // );

            // const check = [EntryFactory.checkbox({
            //     id: 'check',
            //     label: 'checkbox label',
            //     modelProperty: 'check   ',
            //     description: 'teste da descrição da checkbox'
            // })];

            // check.unshift(
            //     EntryFactory.checkbox({
            //         id: 'check',
            //         label: 'teste de checkbox início',
            //         modelProperty: 'jsonSaida'
            //     })
            // );

            // EntryFactory.label({
            //     id: 'texto',
            //     labelText: 'Label de texto teste'
            // });

            return [
                {
                    id: 'tab',
                    label: 'Início',
                    groups: [
                        { id: 'inicio', label: '', entries }
                    ]
                }
            ];
        }
        if ('bpmn:Task'. toLowerCase() === element.type.toLowerCase()) {
            const entries = [];
            // Criação de input - textarea
            const text = [EntryFactory.label({
                id: 'nome',
                labelText: 'Elemento Tarefa'
            })];

            text.unshift(
                EntryFactory.textBox({
                    id: 'componenteTarefa',
                    label: 'teste de texto tarefa',
                    modelProperty: 'jsonSaida'
                })
            );
            // console.log('teste de valor: ', EntryFactory.);
            entries.push(text[0]);

            // Criação de input
            const textField = [EntryFactory.textField({
                id: 'textF',
                label: 'teste de input tarefa',
                modelProperty: 'textF',
                description: null,
            })];
            entries.push(textField[0]);
            // console.log('textField: ', textField);

            // Criação de label
            const lbl = [EntryFactory.label({
                id: 'text',
                labelText: 'Label teste de  Tarefa'
            })];
            entries.push(lbl[0]);

            // Criação do input - dropdownlist
            const select = [EntryFactory.selectBox({
                id: 'sel',
                label: 'Método HTTP',
                modelProperty: 'sel',
                selectOptions: [
                    { name: 'Selecione...', value: null },
                    { name: 'GET', value: 'GET' },
                    { name: 'POST', value: 'POST' },
                    { name: 'PUT', value: 'PUT' },
                    { name: 'DELETE', value: 'DELETE' },
                ]
            })];
            entries.push(select[0]);

            // Criação do input - checkbox
            const check = [EntryFactory.checkbox({
                id: 'che',
                label: 'Checkbox de Tarefa',
                description: null,
                canBeHidden: null,
                canBeDisabled: null
            })];
            entries.push(check[0]);

            // Criação do input - comboBox
            // const combo = [EntryFactory.comboBox({
            //     id: 'combo',
            //     label: 'Combo teste',
            //     customName: '',
            //     customValue: '',
            // })];
            // entries.push(combo[0]);

            // Criação do item - table
            // const table = [EntryFactory.table({
            //     id: 'table',
            //     modelProperties: '',
            //     labels: null,
            //     description: null
            // })];
            // entries.push(table[0]);

            // Criação do item - link
            const link = [EntryFactory.link({
                id: 'link',
                label: 'Link tarefa',
                // false esconde o link | true exibe o link
                showLink() { return true; },
                handleClick() {
                    // esse método serve para escolher o que será feito ao clicar no link inserido - exemplo de navegação para outro site
                    window.open('https://github.com/bpmn-io/bpmn-js-examples', '_blank');
                }
            })];

            // console.log('link: ', link[0]);
            entries.push(link[0]);

            return [
                {
                    id: 'tab',
                    label: 'Tarefa',
                    groups: [
                        // {
                        //     id: 'tarefa',
                        //     label: '',
                        //     entries: lbl
                        // },
                        // {
                        //     id: 'select-tarefa',
                        //     label: '',
                        //     entries: select
                        // }
                        { id: 'tarefa', label: 'Ações de Tarefa:', entries }
                    ]
                }
            ];
        }

        if ('bpmn:ExclusiveGateway'.toLowerCase() === element.type.toLowerCase()) {
            const entries = [EntryFactory.label({
                id: 'nome',
                labelText: 'Elemento Portão'
            })];

            entries.unshift(
                EntryFactory.textBox({
                    id: 'componentePortao',
                    label: 'teste de texto portão',
                    modelProperty: 'jsonSaida'
                })
            );
            return [
                {
                    id: 'tab',
                    label: 'Portão',
                    groups: [
                        {  id: 'portao', label: '', entries }
                    ]
                }
            ];
        }

        if ('bpmn:EndEvent'.toLowerCase() === element.type.toLowerCase()) {
            const entries = [EntryFactory.label({
                id: 'nome',
                labelText: 'Elemento Final'
            })];

            entries.unshift(
                EntryFactory.textBox({
                    id: 'componenteFinal',
                    label: 'teste de texto final',
                    modelProperty: 'jsonSaida'
                })
            );
            return [
                {
                    id: 'tab',
                    label: 'Final',
                    groups: [
                        { id: 'final', label: '', entries }
                    ]
                }
            ];
        }

        return [
            // Aba - início
            {
                id: 'inicio',
                label: 'Início',
                groups: [
                    { id: 'inicio',  label: '' }
                ]
            },
            // Aba - tarefa
            {
                id: 'tarefa',
                label: 'Tarefa',
                groups: [
                    { id: 'tarefa', label: '' },
                    // { id: 'select-tarefa',  label: '' }
                ]
            },
            // Aba - portão
            {
                id: 'gate',
                label: 'Portão Exclusivo',
                groups: [
                    { id: 'portao', label: '' }
                ]
            },
            // Aba - final
            {
                id: 'end',
                label: 'Final',
                groups: [
                    { id: 'final', label: '' }
                ]
            }
        ];
    }
}
