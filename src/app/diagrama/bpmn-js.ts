import PaletteProvider from 'bpmn-js/lib/features/palette/PaletteProvider';
import Modeler from 'bpmn-js/dist/bpmn-modeler.production.min.js';
import * as _CamundaPropertiesProvider from 'bpmn-js-properties-panel/lib/provider/camunda';
import * as _ElementTemplates from 'bpmn-js-properties-panel/lib/provider/camunda/element-templates';
import * as _PropertiesPanelModule from 'bpmn-js-properties-panel';
import * as _PropertiesProviderModule from 'bpmn-js-properties-panel/lib/provider/camunda';
import * as _EntryFactory from 'bpmn-js-properties-panel/lib/factory/EntryFactory';
import * as _ModelUtil from 'bpmn-js/lib/util/ModelUtil';
import _PropertiesActivator from 'bpmn-js-properties-panel/lib/PropertiesActivator';
// @ts-ignore
import * as _CamundaModdleDescriptor from 'camunda-bpmn-moddle/resources/camunda.json';

/**
 * Classe utilizada para importar as depencias do BPMN_JS.
 */
export const InjectionNames = {
    eventBus: 'eventBus',
    bpmnFactory: 'bpmnFactory',
    entryFactory: 'entryFactory',
    elementFactory: 'elementFactory',
    elementRegistry: 'elementRegistry',
    translate: 'translate',
    moddle: 'moddle',
    bpmnModdle: 'bpmnModdle',
    propertiesProvider: 'propertiesProvider',
    propertiesActivator: 'propertiesActivator',
    modelUtil: 'modelUtil',
    elementTemplates: 'elementTemplates',
    palleteProvider: 'palleteProvider',
    originalPaletteProvider: 'originalPaletteProvider'
};

export const EntryFactory = _EntryFactory;
export const BpmnModeler = Modeler;
export const OriginalPaletteProvider = PaletteProvider;
export const CamundaPropertiesProvider = _CamundaPropertiesProvider;
export const ElementTemplates = _ElementTemplates;
export const PropertiesPanelModule = _PropertiesPanelModule;
export const PropertiesPanelProvider = _PropertiesProviderModule;
export const PropertiesActivator = _PropertiesActivator;
export const ModelUtil = _ModelUtil;
export const CamundaModdleDescriptor = _CamundaModdleDescriptor;

export interface IPaletteProvider {
    getPalleteEntries(): any;
}

export interface IPalette {
    registerProvider(provider: IPaletteProvider): any;
}

export interface IPropertiesProvider {
    getTabs(elemnt): any;
}
