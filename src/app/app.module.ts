import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {NgBpmnEditorModule} from 'ng-bpmn';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DiagramaComponent} from './diagrama/diagrama.component';


// @ts-ignore
@NgModule({
    declarations: [
        AppComponent,
        DiagramaComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgBpmnEditorModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
