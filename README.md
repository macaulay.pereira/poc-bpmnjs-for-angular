# POC - BPMN-JS

### Instalação de dependências
Execute `npm install --save bpmn-js@3.4.1` .

Execute `npm install --save bpmn-js-properties-panel@0.30.0` .

Execute `npm install --save camunda-bpmn-moddle@3.0.0` .

Estamos importando todas as dependências no arquivo `bpmn-js.ts`, dessa forma conseguimos deixar todas as adaptações de import do JS puro para o Angular em um só arquivo e basta referenciar o arquivo assim que necessário usar alguma dessas Bibliotecas.

OBS: É necessário referenciar as stylesheets do BPMN-JS nos styles do `angular.json` .

### Baixar dependências
Execute `npm install` .

### Servidor Local

Execute `ng serve` para subir o servidor local. Navegue até `http://localhost:4200/`. O app irá atualizar a página automaticamente caso você altere qualquer um dos arquivos fonte.
